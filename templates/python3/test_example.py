import unittest
from example import example

class TestExample(unittest.TestCase):
    def test_example_should_return_true(self):
        self.assertEquals(example("aString"), True)
