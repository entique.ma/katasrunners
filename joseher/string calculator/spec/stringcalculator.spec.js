import { stringcalculator } from '../src/stringcalculator.js'

describe('String Calculator', () => {
    it('should return 0 for an empty string', () => {
        // Arrange
        const expected_value = 0

        // Act
        const result = stringcalculator("")

        // Assert
        expect(result).toBe(expected_value)
    })
    it('should return the number for an string with only one number', () => {
        // Arrange
        const expected_value = 1

        // Act
        const result = stringcalculator("1")

        // Assert
        expect(result).toBe(expected_value)
    })
    it('should return the sum of the numbers', () => {
        // Arrange
        const expected_value = 3

        // Act
        const result = stringcalculator("1,2")

        // Assert
        expect(result).toBe(expected_value)
    })
    it('should handle any amount of numbers', () => {
        // Arrange
        const expected_value = 10

        // Act
        const result = stringcalculator("1, 2, 3, 4")

        // Assert
        expect(result).toBe(expected_value)
    })
})
