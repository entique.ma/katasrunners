# Roman Numerals Converter

## Explicación del problema

Escribirás un programa que convierta de números enteros a números romanos.

### Enlaces relevantes

- (Roman numerals)[https://www.mathsisfun.com/roman-numerals.html]
- (list.index)[https://www.tutorialspoint.com/python/list_index.htm]

## Pistas

### Pista 1

Crear dos listas, una con los numeros romanos y otra con sus equivalencias decimales te será de mucha ayuda.

## Pista 2

Si añades los números a la lista que van antes que la nueva letra sea introducida, valores como 4, 9 o 40, te ahorrará mucho código.

## Pista 3

No puedes tener más de tres números romanos iguales consecutivos.
