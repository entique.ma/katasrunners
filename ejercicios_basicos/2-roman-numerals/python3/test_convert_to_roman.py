import unittest
from convert_to_roman import convertToRoman

class TestConvertToRoman(unittest.TestCase):
    def test_a_convert_single_number(self):
        self.assertEqual(convertToRoman(1), "I")

    def test_b_uses_same_symbol_multiple_times(self):
        self.assertEqual(convertToRoman(3), "III")

    def test_c_combines_two_symbols_for_numbers_smaller_than_nine(self):
        self.assertEqual(convertToRoman(8), "VIII")

    def test_d_combines_three_symbols_for_numbers_smaller_than_forty(self):
        self.assertEqual(convertToRoman(38), "XXXVIII")

    def test_e_combines_all_symbols_to_convert_numbers(self):
        self.assertEqual(convertToRoman(3256), "MMMCCLVI")

    def test_f_ensures_a_symbol_is_not_repeated_more_than_three_times(self):
        self.assertEqual(convertToRoman(4), "IV")
