export function checkCashRegister(price, cash, cid) {
    const VALOR_MONEDAS = [
        ["PENNY", 0.01],
        ["NICKEL", 0.05],
        ["DIME", 0.1],
        ["QUARTER", 0.25],
        ["ONE", 1],
        ["FIVE", 5],
        ["TEN", 10],
        ["TWENTY", 20],
        ["ONE HUNDRED", 100]
    ]
    var change = { status: "", change: [] };
    var aDevolver = cash - price;
    aDevolver = aDevolver.toFixed(2);
    var totalEnCaja=0;
    for(var x=0; x<cid.length; x++) {
        totalEnCaja += cid[x][1];
    }
    totalEnCaja = totalEnCaja.toFixed(2);
    aDevolver = parseFloat(aDevolver);
    totalEnCaja = parseFloat(totalEnCaja);

    if(aDevolver > totalEnCaja){
        change = { status: "INSUFFICIENT_FUNDS", change: [] };
        return change;
    }

    if(aDevolver === 0.00) {
        change = { status: "OPEN", change: [] };
        return change;
    }
    
    if(aDevolver == totalEnCaja){
        change.status = "CLOSED";
        for(var x=cid.length-1; x>=0; x--) {
            if(aDevolver>=VALOR_MONEDAS[x][1]) {
                change.change[x] = [VALOR_MONEDAS[x][0], 0];
                while(aDevolver>=VALOR_MONEDAS[x][1]) {   
                    aDevolver -= VALOR_MONEDAS[x][1];
                    aDevolver = aDevolver.toFixed(2);
                    change.change[x][1] = change.change[x][1] + VALOR_MONEDAS[x][1];
                }
            }
        }
        return change;
    }

    if(aDevolver < totalEnCaja){
        change.status = "OPEN";
        for(var x=cid.length-1; x>=0; x--) {
            if(aDevolver>=VALOR_MONEDAS[x][1]) {
                change.change.push([VALOR_MONEDAS[x][0], 0]);
                while( aDevolver>=VALOR_MONEDAS[x][1] && change.change[change.change.length-1][1] < cid[x][1] ) {   
                    aDevolver = aDevolver - VALOR_MONEDAS[x][1];
                    aDevolver = aDevolver.toFixed(2);
                    change.change[change.change.length-1][1] = change.change[change.change.length-1][1] + VALOR_MONEDAS[x][1];
                }
            }
        }
        return change;
    }
    // Here is your change, ma'am.
    
}